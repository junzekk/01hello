# 定义函数
def aa():
    print("1:aaaa")
    print("2:aaaa")
    print("3:aaaa")
# 调用函数
# aa()

def stu(name='zhangsan',age=20,sex='man'):
    print("姓名:{},年龄:{},性别:{}".format(name,age,sex))

stu()
stu("lisi")
stu("wangwu",37)
stu("zhaoliu",40,'women')
stu("zhaoliu",'women',40)
stu("zhaoliu",sex='women',age=40)

def demo(*arg):
    print(arg)

demo(10,20)

def demo1(**arg):
    print(arg)

demo1(name='zhangsan',age=20)

def demo2(m,**arg):
    print(m)
    print(arg)

demo2(10,name='zhangsan',age=20)

sum=lambda v1,v2:v1+v2

print(sum(10,20))