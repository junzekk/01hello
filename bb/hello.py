# a = 1
# print(a)

# # while 循环
# i = 1
# while i<=10:
#     print(i)
#     i+=1
#
# # 1~100相加
# i = 1
# sum=0
# while i<=10:
#     sum+=i
#     i+=1
# print(sum)
# 死循环
while True:
    k=input("输入一个值：")
    print("=="+k)
    if k=="q":
        print("退出！")
        break