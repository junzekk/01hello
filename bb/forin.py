#
# # 遍历字符串
# for c in "abcd":
#     print(c)
#
# # 遍历列表
# for c in [10,20,30]:
#     print(c)
# # 遍历元组
# for c in (10,20,30):
#     print(c)
# # 遍历集合
# for c in {10, 20, 30}:
#     print(c)
# # 遍历字典
# a={'name':"list",'age':20}
# for i in a:
#     print(i,":",a[i])
# #嵌套结构
# a=[('a','aaaa'),('b','bbbb'),('c','cccc')]
# for v1,v2 in a:
#     print(v1,"=>",v2)
#
# range()使用
# 输出0-9
for i in range(10):
    print(i,end=" ")
print("")
# 输出2-6
for i in range(2,7):
    print(i,end=" ")
print("")
# 输出0-50,递增5
for i in range(0,51,5):
    print(i,end=" ")
print("")
# 10~1
for i in range(10,0,-1):
    print(i,end=" ")
print("")
a=["aaa","bbb","ccc"]
for i in range(len(a)):
    print(i,"=>",a[i])