# 写程序打印杨辉三角形（只打印6层即可）
#           1
#        1    1
#       1   2  1
#      1  3  3  1
#     1  4  6  4 1
#    1 5  10 10 5 1
# 杨辉三角形：第n行的第n个元素 == 第n-1行的第n个元素和第n-1个元素的和

# 生成杨辉三角形每行元素的迭代器
def Pascal_Triangle_In_line(afterLine,n):
    # 生成器函数。用来生成杨辉三角形第n行的第m个元素
    for x in range(n):
        if x ==0:
            yield 1
        else:
            yield sum(afterLine[x-1:x+1])
# 生成第n行的杨辉三角形
def print_Pascal_Triangle_line(afterline,n):
    '''
    用于生成杨辉三角形一行内的每一个元素并把这些元素的用list的形式返回
    :param afterline:
    :param n:
    :return:
    '''
    it = iter(Pascal_Triangle_In_line(afterline,n))
    L =list()
    while True:
        try:
            L.append(next(it))
        except StopIteration:
            break
    return L
# 生成n行的杨辉三角形
def print_Pascal_Triangle(n):
    '''
    是一个生成器函数
    :param n: 用于接受需要打印多少行的杨辉三角形
    :return:
    '''
    List = [1]
    for x in range(n):
        List = print_Pascal_Triangle_line(List,x+1)
        yield List
# 此处使用迭代迭代生成杨辉三角形
it = iter(print_Pascal_Triangle(6))
for x in it:
    print(" ".join(str(x).lstrip("[").rstrip("]").split(",")).center(20))