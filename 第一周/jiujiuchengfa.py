# 九九乘法表
# 循环输出1~9的值
for i in range(1,10):
    print(i,end=" ")
# 循环九行
for j in range(1,10):
    print("")

# 九行
for j in range(1,10):
    for i in range(1,10):
        print(i,end=" ")
    print("")
"""
# 三角形
for j in range(1, 10):
    for i in range(1,j+1):
        print(i, end=" ")
    print("")
"""
# 九九乘法表
# :2  占位 :<4 占位靠左
for j in range(1, 10):
    for i in range(1,j+1):
        print("{}*{}={:2}".format(i,j,i*j), end=" ")
    print("")
# 九九乘法表 倒叙
for j in range(9,0,-1):
    for i in range(1, j + 1):
        print("{}*{}={:2}".format(i, j, i * j), end=" ")
    print("")

# 使用while循环的九九乘法表
j=1
while j<=9:
    i=1
    while i<=j:
        print("{}*{}={:<4}".format(i,j,i*j),end=" ")
        i+=1
    print("")
    j=j+1


a=[10,20,30,40]
m=100
for i in a:
    if m==i:
        print("存在")
        break
else:
    print("不存在")