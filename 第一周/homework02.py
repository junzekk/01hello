'''
# 2) 使用文件和目录操作，定义一个统计指定目录大小的函数（注意目录中还有子目录）。
'''
import os
def dirsize(path):
    '''
    计算文件夹的大小
    :param path:
    :return:
    '''
    sumsize=0
    ld=os.listdir(path)
    for i in ld:
        if os.path.isdir(os.path.join(path,i)):
            sumsize=sumsize+dirsize(os.path.join(path,i))
        else:
            sumsize=sumsize+os.path.getsize(os.path.join(path,i))
    return sumsize

print(dirsize("./aa"))