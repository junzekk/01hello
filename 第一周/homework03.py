'''
# 3) 参考第9节的综合案例中的学生信息管理，来实现一个自动取款机的存取款模拟效果。要求有登陆和退出、查询余额、取钱，存钱等操作。
'''
#模拟储蓄卡
card={'money':1000,'pwd':123456}
def showMoney():
    '''
    显示余额
    :return:
    '''
    print("------------余额 ------------")
    print("-----------",card['money'],"-----------")
print("="*12,"取款机系统","="*14)
print("="*12,"  请登录  ","="*14)
password=input("请输入密码")

if int(password)==card['pwd']:
    while True:
        print("{0:1} {1:13} {2:15}".format(" ","1.查看余额","2.存款"))
        print("{0:1} {1:13} {2:15}".format(" ","3.取款","4.退出系统"))
        print("="*40)
        key = input("请输入对应选项")
        if key == "1":
            print("="*12,"查看余额","="*14)
            showMoney()
            input("按回车键继续：")
        elif key=="2":
            print("=" * 12, "存款", "=" * 14)
            save=input("请输入存多少")
            card['money'] = card['money'] + int(save)
            showMoney()
            input("按回车键继续：")
        elif key=="3":
            print("=" * 12, "取款", "=" * 14)
            out=input("请输入取多少")
            card['money']=card['money']-int(out)
            if card['money'] <0:
                print("你的余额不足")
            else:
                showMoney()
                print("取款成功")
            input("按回车键继续：")
        elif key=="4":
            print("=" * 12, "退出系统", "=" * 14)
            break
        else:
            print("============无效输入============")
else:
    print("="*12,"密码错误，请取卡","="*14)