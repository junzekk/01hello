# a = 1
# print(a)

# # while 循环
# i = 1
# while i<=10:
#     print(i)
#     i+=1
#
# # 1~100相加
# i = 1
# sum=0
# while i<=10:
#     sum+=i
#     i+=1
# print(sum)
# 死循环
# while True:
#     k=input("输入一个值：")
#     print("=="+k)
#     if k=="q":
#         print("退出！")
#         break
r = r"hello I\'m python"
print(r)

l1=["abc", 1, [list]]
# isinstance()
print(type(l1))
for i in l1:
    if isinstance(i,list):
        print(type(i))

age = 20
if age<=16:
    print("teenager")
elif 16<age<=18:
    print("adult")
elif 18<age<=30:
    print('中年')
else:
    print("老年")