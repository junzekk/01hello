#学员信息管理
#定义存放学生信息列表
stulist=[
    {'name':'zhangsan','age':20,'classid':"py03"},
    {'name':'lisi','age':21,'classid':"py02"},
    {'name':'wangwu','age':22,'classid':"py03"},
    {'name':'zhaoliu','age':23,'classid':"py02"}
]
#信息输出函数
def showStu(stulist):
    if len(stulist)==0:
        print("============ 没有学员信息 ===============")
    print("|{0:<5}| {1:<10} |{2:<5} |{3:<10} |".format("sid","name","age","classid"))
    print("-"*40)
    for i in range(len(stulist)):
        print("|{0:<5}| {1:<10} |{2:<5} |{3:<10} |".format(i+1,stulist[i]['name'],stulist[i]['age'],stulist[i]['classid']))
while True:
    print("="*12,"学员管理系统","="*14)
    print("{0:1} {1:13} {2:15}".format(" ","1.查看学员信息","2.添加学员信息"))
    print("{0:1} {1:13} {2:15}".format(" ","3.删除学员信息","4.退出系统"))
    print("="*40)

    key = input("请输入对应选项")
    if key=="1":
        print("="*12,"学员信息浏览","="*14)
        showStu(stulist)
        input("按回车键继续：")
    elif key=="2":
        print("=" * 12, "学员信息添加", "=" * 14)
        stu={}
        stu['name']=input("请输入添加的姓名")
        stu['age']=input("请输入添加的年龄")
        stu['classid']=input("请输入添加的课程")
        stulist.append(stu)
        showStu(stulist)
        input("按回车键继续：")
    elif key=="3":
        print("=" * 12, "学员信息删除", "=" * 14)
        showStu(stulist)
        did=input("请输入要删除的id号")
        del stulist[int(did)-1]
        showStu(stulist)
        input("按回车键继续：")
    elif key=="4":
        print("=" * 12, "退出系统", "=" * 14)
        break
    else:
        print("============无效输入============")