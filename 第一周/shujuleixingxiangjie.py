# print(abs(10))
# print(abs(-10))
# # print(max())
#
# print("as\\df\"ad\nsf")
#
# a="hello"
# b="world"
# print(a+b)
# print(a*2)
#
# print(a[0])
# print(a[0:3])
#
# print('lo' in a)
# print("wa" not in b)
# print(r"aa\\\"bb")
'''
# %s 字符串占位符 %d 数字占位符
print("name:%s age:%d"%('zhangsan',20))
print("4.56789=>%0.f"%(4.56789))
print("4.56789=>%0.2f"%(4.56789))
print("%x"%(255)) #16进制



name="zhangsan"
print(len(name))
print(max(name))
print(min(name))
print(name.lower())
print(name.upper())
print(name.count('an'))
print(name.replace("san","ddddd"))
print("10:20:30".split(":"))
print(name.find('an'))
print(name.rfind('an'))
print(name.index('an'))
print(name.strip())


# 新建列表方式
list0=[]
list1=list()

list2=[10,20,30]
print(list2[1])
print(list2[1:])
list2[2]=300
del list2[1]
print(list2)

# 遍历列表
list3=[10,20,30]

for i in list3:
    print(i)

i=0
while i<len(list3):
    print(list3[i])
    i+=1

list4=[[10,20],[30,40],[50,60]]
for v1,v2 in list4:
    print(v1,v2)

#嵌套长度不同的列表
list5=[[10,20],[30,40,50,60],[50,60,70]]
for i in list5:
    for j in i:
        print(j,end=" ")
    print()


list6=[10,20,30,40,50,60]
list6.append(60)
print(list6)
list6.extend(["aa","bb","cc"])
print(list6)
list6.insert(2,"bb")
print(list6)
print(list6.count("bb"))
print(list6.index("bb"))
print(list6.index(20))
# list6.pop("bb") worng
print(list6)
list6.pop(3)
print(list6)
list6.remove("aa")
print(list6)
list6.reverse()
print(list6)
list7=[50,10,20,40,60,30]
list7.sort()
print(list7)


#元组
# 新建元组
t0=()
t1=tuple()
# 定义元组(常用t3)
t3=(1,2,3,4,5)
t4="a","b","c","d"
print(t0)
print(t3[1:3])

tt=(50) #这是int类型
ttt=(50,)#这是元组

# 不能修改，只能删除
del ttt

# 遍历
for i in t3:
    print(i)
print(t3+t4)   #可以相加
print(t3*2)

# 其他
print(max(t3))
print(min(t3))
print(2 in t3)
#转成元组
print(tuple("abcd"))


# Set集合
# 新建集合,空的花括号是字典
s0=set()
s1={1,2,3,4}

#添加    重复的不会添加
s1.add(5)
s1.update([10,20,30])   #添加多个
# 删除
s1.remove(1)
s1.discard(100)#删除不存在的不报错

# 遍历
for i in s1:
    print(i)

# 其他
print(len(s1))
'''
# Dictionary字典
# 新建字典,空的花括号是字典
d0={}
d1=dict()

d1['name']="lisi"
print(d1)
d2={"name":'zhangsan','age':'20','sex':'man'}
print(d2['age'])

# 修改
d2['age']=22
d2['phone']='123456'
print(d2)
# 删除
del d2['phone']
print(d2)

# 遍历
for k in d2:
    print(k,"==>",d2[k])

d2.items() #列出键值对
for k,v in d2.items():
    print(k,"==>" ,v)

#推导式
a={k:v for k,v in d2.items()}
print(a)

# 其他
print('age' in d2)

#内置方法
c= dict.fromkeys(('a','b','c'),100) #快速组装字典
print(c)

a= d2.copy() #复制
print(a)

a.clear()#清空

print(d2.get('age'))
print(d2.get('age',20)) #没有选默认值

print(d2.keys())
print(d2.values())

d2.update({'age':24,'sex':'woman'})
print(d2)
d2.pop('sex')
print(d2)
