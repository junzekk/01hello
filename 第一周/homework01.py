'''
# 1) 使用while和for…in两个循环分别输出四种九九乘法表效果（共计8个）。

解题提示
a) 参考第7节内容九九乘法表建议使用format格式化输出

'''

# 1)九九乘法表
#使用for in
for j in range(1, 10):
    for i in range(1,j+1):
        print("{}*{}={:2}".format(i,j,i*j), end=" ")
    print("")
# 九九乘法表 倒叙
for j in range(9,0,-1):
    for i in range(1, j + 1):
        print("{}*{}={:2}".format(i, j, i * j), end=" ")
    print("")

for i in range(1,10):
    for j in range(1,10-i): #输入一个空的三角形
        print(end="       ")
    for k in range(1,i+1):
        print("{}*{}={:2}".format(i,k,i*k),end=" ")
    print("")

for i in range(9,0,-1):
     for k in range(9, i,-1):
        print(end="       ")
     for j in range(1, i+1):
        print("{}*{}={:2}".format(i, j, i*j), end=" ")
     print(" ")
# 使用while
j = 1
while j <= 9:
    i = 1
    while i <= j:
        print("{}*{}={:<4}".format(i, j, i * j), end=" ")
        i += 1
    print("")
    j = j + 1

j = 9
while j <= 9:
    i = 1
    if j < 1:
        break
    while i <= j:
        print("{}*{}={:<4}".format(i, j, i * j), end=" ")
        i += 1
    print("")
    j = j - 1

j=1
while j<=9:
    k=1
    while k<=9-j:
        print("    ",end="    ")
        k=k+1
    i=1
    while i<=j:
        print("{}*{}={:<4}".format(i,j,i*j),end="")
        i=i+1
    j=j+1
    print("")
j=9
while j>=1:
    k=1
    while k<=9-j:
        print("    ", end="    ")
        k=k+1
    i=1
    while i <= j:
        print("{}*{}={:<4}".format(i, j, i * j), end="")
        i += 1
    print("")
    j = j - 1

