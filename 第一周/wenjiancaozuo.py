import os
#python文件操作

'''
# 1.打开文件
file = open("./a.txt","r")
#读取文件内容输出
# content = file.read()
content = file.read(5) #读取5个字节
print(content)
# 关闭
file.close()

# 2.readline读取一行
file = open("./a.txt","r")
content=file.readline()
while len(content)>0:
    print(content,end='')
    content=file.readline()
# print(content)
file.close()

#3 readlines()
file = open("./a.txt","r")
flist=file.readlines()
for line in flist:
    print(line,end='')
file.close()

#写入操作
file = open("./b.txt","w") #w 覆盖写，a追加写
file.write("hello python!\n")
file.write("hello world!\n")

file.close()



path = os.getcwd()
print(path)
dirfile = os.listdir()
print(dirfile)
#创建文件夹
os.mkdir("bb")  #创建文件夹
#重命名文件夹
os.rename("bb","cc")
#删除文件夹，里面有文件回报错，只能删除空的文件夹
os.rmdir("cc")
#文件状态
info=os.stat("a.txt")
print(info.st_size)


print(os.getenv("PATH"))
print(os.curdir)#当前目录
print(os.path) #<module 'ntpath' from 'D:\\application\\Python\\lib\\ntpath.py'>
print(os.name)#nt
print(os.sep) #分隔符
print(os.path.exists("a.txt"))
file = "a/b/c/d/e.txt"
print(os.path.basename(file)) #basename 文件名
print(os.path.dirname(file)) #dirname 目录名
path="a/b/c"
file1 = "d.txt"
# print(os.path.join(path,file1)) #a/b/c\d.txt
print(os.path.join(path,"/",file1)) #/d.txt
print(os.path.getsize("forin.py")) #只能算文件，算不了文件夹，只能累加算出整个文件夹
'''

#自定义文件复制函数
def mycopy(fromfile,tofile):
    #打开文件元和目标文件
    f1 = open(fromfile,"rb")  #rb，wb代表用二进制打开,用于图片音视频
    f2 = open(tofile,"wb")
    #循环读取并写入，
    content = f1.readline()
    while len(content)>0:
        f2.write(content)
        content=f1.readline()

    #关闭源和目标文件
    f1.close()
    f2.close()
#测试
# mycopy("./a.txt","./c.txt")
# mycopy("./a.jpeg","./c.jpeg")


#复制文件夹实战
def copydd(dir1,dir2):
    #获取被复制目录中的所有文件信息
    dlist = os.listdir(dir1)
    #创建新目录
    os.mkdir(dir2)
    #遍历所有文件，并执行文件复制
    for f in dlist:
        file1 = os.path.join(dir1,f)
        file2 = os.path.join(dir2,f)
        if os.path.isfile(file1):
            mycopy(file1,file2)
        if os.path.isdir(file1): #文件夹里有 文件夹，使用递归
            copydd(file1,file2)

# 测试
# copydd("./aa","./bb")

#作业 ：统计目录大小

def dirsize(path):
    sumsize=0
    ld=os.listdir(path)
    for i in ld:
        if os.path.isdir(os.path.join(path,i)):
            sumsize=sumsize+dirsize(os.path.join(path,i))
        else:
            sumsize=sumsize+os.path.getsize(os.path.join(path,i))
    return sumsize

print(dirsize("./aa"))