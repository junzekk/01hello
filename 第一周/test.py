'''
a=["aaa","bbb","ccc"]
for i in a :
    print(i)

a=["aaa","bbb","ccc"]
for i in range(len(a)) :
    print(i,"=>",a[i])

for i in range(1,10):
    for j in range(1,i+1):
        print("{:2}*{:2}={:2}".format(j,i,i*j),end=" ")
    print("")

def demo1(**arg):
    print(arg)

demo1(name='zhangsan',age=20)

list2=[10,20,30]
print(list2[1])
print(list2[1:])
s1={1,2,3,4}

#添加    重复的不会添加
s1.add(5)
print(s1)
s1.update([10,20,30])   #添加多个
print(s1)
c= dict.fromkeys(('a','b','c'),100) #快速组装字典
print(c)

'''

def mycopy(m,n):
    #打开文件
    f1=open(m,"rb")
    f2=open(n,"wb")
    #循环读取
    content=f1.readline()
    while len(content)>0:
        f2.write(content)
        content=f1.readline()

    #关闭文件
    f1.close()
    f2.close()

import os
# mycopy("./c.txt","./d.txt")
def copydd(dir1,dir2):
    #获取被复制目录所有哦信息
    listdir = os.listdir(dir1)
    print("====",listdir)
    #创建被复制目录
    os.mkdir(dir2)
    #循环遍历，执行复制
    for f in listdir:
        file1= os.path.join(dir1,f)
        print("----",file1)
        file2 = os.path.join(dir2,f)
        if os.path.isdir(file1):
            copydd(file1,file2)
        else:
            mycopy(file1,file2)

copydd("./aa","./bb")