#继承
class Animal(object):
    def sleep(self):
        print("睡觉呢")
class Cat(Animal):
    def __init__(self,name,color="白色"):
        self.name=name
        self.color=color
    def run(self):
        print("%s--在跑"%self.name)

class Bosi(Cat):
    def setNewName(self,newName):
        self.name=newName
    def eat(self):
        print("%s--在吃"%self.name)
bosi = Bosi("印度猫","黑色")
print("颜色是：",bosi.color)
print("名字是：",bosi.name)
bosi.eat()
bosi.run()
bosi.setNewName("波斯猫")
print("改名为：",bosi.name)

class base(object):
    def test(self):
        print('----base test----')
class A(base):
    def test(self):
        print('----A test----')

# 定义一个父类
class B(base):
    def test(self):
        print('----B test----')

# 定义一个子类，继承自A、B
class C(B,A):
    pass


obj_C = C()
obj_C.test()

print(C.__mro__) #可以查看C类的对象搜索方法时的先后顺序