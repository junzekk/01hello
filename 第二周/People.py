


#保护对象属性
class People(object):
    #方法
    def __init__(self,name):
        self._name=name  #私有属性，不能直接修改
    def getName(self):
        return self._name
    def setName(self,newName):
        if len(newName)>0:
            self._name=newName
        else:
            print("名字不能为空")
# xiaoming=People("dongge")
# print(xiaoming._name)

class Cat(object):
    def __init__(self,name):
        self._name=name
    def getName(self):
        return self._name
    def setName(self,newName):
        self._name=newName
bosi=Cat("bosi")
bosi.setName("gogo")
print(bosi.getName())

class Student(object):
    def __init__(self,name,age,gender):
        self.__name=name
        self.__age=age
        self.__gender=gender
    def getGender(self):
        return self.__gender
    def setGender(self,gender):
        self.__gender=gender
    def __str__(self):
        return "name="+self.__name+",age="+str(self.__age)+",gender="+self.__gender

s=Student("kong",12,"efemal")
print(s)
s.setGender("male")
print("s===",s.getGender())

