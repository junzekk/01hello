#函数当参数传递给其他函数
def add(x,y,f):
    return f(x)+f(y)

print(add(-5,6,abs))

#内置map函数,传入一个函数，一个Iteratable,Iteratable的元素不断放入函数中
l =[1,2,3,4,5,6,7]
def f(x):
    return x*x

m = map(f,l)


# print(list(m))
#
# for i in m:
#     print(i)

print(next(m))
print(next(m))


#reduce
l = [1,2,3,4,5,6] #把列表拼接123456
from functools import reduce
def f(x,y):
    return x*10 + y
print(reduce(f,l))

# '5436'==>5436
# 1. 每个元素拿出来，转换成对应数字，得到数字序列
# 2. 通过数字徐磊每2个*10，得到整数

def char2num(s):
    digits = {'0':0,'1':1,'2':2,'3':3,'4':4,'5':5,'6':6}
    return digits[s]
s1='5436'
m = map(char2num,s1)
nums = reduce(f,m)
print(type(nums))
print(nums)

'''
匿名函数 在函数中定义函数
'''
DIGITS = {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6}
def str2int(s):
    def f(x,y):
        return x * 10 + y
    def char2num(s):
        return DIGITS[s]
    return reduce(f,map(char2num,s))
print(str2int('2222'))
# lambda表达式
def str2int(s):
    def char2num(s):
        return DIGITS[s]
    return reduce(lambda x,y:x*10+y,map(char2num,s))
print(str2int('2233'))

'''
装饰器
'''
# import datetime
# def now():
#     print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
# # now()
# #装饰器 以一个函数作为参数，并返回一个函数
# def log(f):
#     def write_log(*args,**kwargs):
#         with open('./a.txt','w') as f1:
#             f1.write(f.__name__)
#             print('写入日志成功，他的名字是：%s'%f.__name__)
#             return f(*args,**kwargs)
#     return write_log
# ff = log(now)
# ff()


import datetime
def log(f):
    def write_log(*args,**kwargs):
        with open('./a.txt','w') as f1:
            f1.write(f.__name__)
            print('写入日志成功，他的名字是：%s'%f.__name__)
            # return f(*args,**kwargs)
    return write_log
@log
def now():
    print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
now()

'''
内置的装饰器@property@setter  使对象方法变属性
'''
class Student(object):
    def __init__(self,score):
        self.__score = score

    @property
    def score(self):
        return self.__score

    @score.setter
    def score(self,value):
         self.__score = value

stu = Student(88)
stu.score=56
print(stu.score)

print('==========作业==========')
ll=['MIKE', 'Adidas','coffee']
def f1(x):
    # print( x[0].upper()+x[1:].lower())
    return x[0].upper()+x[1:].lower()
# f1("make")
# print(type(m1))
print(list(map(f1,ll)))
