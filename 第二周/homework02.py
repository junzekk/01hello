# 作业二：
# 请设计一个装饰器，它可以作用于任何函数上，打印函数执行时间：
# import time
# def metric(fn):
# start_time = time.time()
# end_time = time.time()
#
# print('耗时：{:.4f}s'.format(end_time - start_time))
# return fn

import time
def metric(fn):
    def log(*args,**kwargs):
        start_time = time.time()
        r = fn(*args,**kwargs)
        end_time = time.time()
        print('耗时：{:.4f}s'.format(end_time - start_time))
        return r
    return log

@metric
def fun(x,y):
    time.sleep(1)
    return x*y

fun(2,5)