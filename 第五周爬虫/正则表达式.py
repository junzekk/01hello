import re

with open('index.html','r',encoding='utf-8',)as f:
    html = f.read()
    # print(html)
    html = re.sub('\n','',html)
    print(html)
    # pattern_1 = '<div class="email">(.*?)</div>'  这种会报错，为什么
    pattern_1 = "<div class = 'email'>(.*?)</div>"
    ret_1 = re.findall(pattern_1,html)
    # #为什么是空呢？因为.匹配除了\n外的
    # print(ret_1)  #[] 添加这句html = re.sub('\n','',html)
    print(ret_1[0].strip())


# 规则：英文字母开头，可以包括数字、大小写英文字母、下划线，6-16位 ,r防转义
password_pattern = r'^[a-zA-Z][a-zA-Z0-9_]{5,15}$'
pass1 = '1234567'
pass2 = 'k12345'
pass3 = 'k123'

# print(re.match(password_pattern,pass1))
# print(re.match(password_pattern,pass2))
# print(re.match(password_pattern,pass3))


